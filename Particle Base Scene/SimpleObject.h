
#ifndef SIMPLEOBJECT_H
#define SIMPLEOBJECT_H

#include <glm/glm.hpp>

struct SimpleObject
{
	glm::vec3 position;
	glm::vec3 scale;
	//glm::vec3 rotation;

	SimpleObject(glm::vec3 position = glm::vec3(0.0f,0.0f,0.0f), glm::vec3 scale = glm::vec3(1.0f,1.0f,1.0f))//, glm::vec3 rotation = glm::vec3(0.0f,0.0f,0.0f))
	{
		this->position = position;
		this->scale = scale;
		//this->rotation = rotation;
	}
};

#endif